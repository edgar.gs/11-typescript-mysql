import mysql = require('mysql');

export default class MySQL {

    private static _instance: MySQL;

    cnn: mysql.Connection;
    //connected: boolean = false;

    constructor(){
        this.cnn = mysql.createConnection({
            host: 'localhost',
            user: 'user',
            password: 'password',
            database: 'node_db'
        });

        this.conectarDB();
    }

    public static get instance() {
        return this._instance || (this._instance = new this());
    }

    static ejecutarQuery(query: string, callback: Function){
        this.instance.cnn.query(query,(err, results:Object[], fields)=>{
            if(err){
                return callback(err);
            }

            if(results.length === 0){
                callback('No existe');
            } else{
                callback(null,results);
            }
        });
    }

    private conectarDB() {
        this.cnn.connect( (err: mysql.MysqlError) => {
            if(err){
                return console.log(err.message);
            }

            console.log('BBDD conectado');
        });
    }
}